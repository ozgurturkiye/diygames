from django.urls import path
from . import views


urlpatterns = [
    path('', views.welcome, name='welcome'),
    path('milyoner/', views.index, name='index'),
    path('about-me/', views.about_me, name='about-me'),
    path('milyoner/questions/', views.QuestionListView.as_view(), name='questions'),
    path('milyoner/start-game/', views.StartGameView.as_view(), name='start-game'),
    path('milyoner/<int:pk>', views.QuestionDetailView.as_view(), name='question-detail'),
    path('milyoner/show-question/<soru_id>/', views.show_question, name='show-question'),
    path('milyoner/check-answer/<question_id>/<user_choice>/', views.check_answer, name='check-answer'),
    path('milyoner/app-promo/', views.app_promotion, name='app-promo'),
]
