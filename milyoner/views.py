from django.shortcuts import render
from django.views import generic
from .models import Question
import random


def index(request):
    """
    View function for home page of site.
    """
    # Render the HTML template index.html
    return render(
        request,
        'index.html',
    )


class QuestionListView(generic.ListView):
    """
    Generic class-based view for a list of questions.
    """
    model = Question


class QuestionDetailView(generic.DetailView):
    """
    Generic class-based detail view for a question.
    """
    model = Question

class StartGameView(generic.ListView):
    """
    Generic class-based detail view for a question.
    """
    def get_queryset(self):
        self.question_easy = Question.objects.filter(difficulty='1')
        self.question_easy = random.sample(list(self.question_easy), 3)  #take 3 sample in list but its not working

        self.question_normal = Question.objects.filter(difficulty='2')
        self.question_normal = random.sample(list(self.question_normal), 3)

        self.question_hard = Question.objects.filter(difficulty='3')
        self.question_hard = random.sample(list(self.question_hard), 3)

        self.queryset = self.question_easy + self.question_normal + self.question_hard # make new queryset

        return self.queryset

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        self.num_visits = self.request.session.get('num_visits', 0) #create session if not exist
        self.request.session['num_visits'] = self.num_visits + 1
        context['num_visits'] = self.request.session['num_visits']

        # seçilen soruların id liteleri alınır
        id_listesi = []
        for i in context['object_list']:
            id_listesi.append(i.pk)

        # create a session for id_liste
        self.request.session['id_listesi'] = id_listesi

        context['soru_liste'] = context['object_list'].copy()
        context['id_listesi'] = id_listesi
        # Set session as modified to force data updates/cookie to be saved.
        self.request.session.modified = True
        return context


    #question_easy = Question.objects.filter(difficulty='1')[:3]

    template_name = 'start-game.html'
    context_object_name = 'kedi' #eğlence amaçlı :)


def show_question(request, soru_id):
    """
    View function for home page of site.
    """
    # Generate counts of some of the main objects
    num_questions = Question.objects.all().count()

    # show active question in session and delete question
    active_question = request.session['id_listesi'][0]
    del request.session['id_listesi'][0]
    request.session.modified = True

    if active_question is None:
        return render(request, "no-question.html")
    else:
        question = Question.objects.get(pk=active_question)
        return render(
            request,
            'show-question.html',
            context={'question': question,
                     'num_questions': num_questions,
                     'soru_liste': request.session['id_listesi'],
                     'active_question': active_question},
        )


def check_answer(request, question_id, user_choice):
    """
    View function for home page of site.
    """
    active_question = Question.objects.get(pk=question_id)
    next_question_id = int(question_id) + 1
    if active_question.correct_choice == user_choice:
        result = "dogru"
    else:
        result = "yanlis"
    # Render the HTML template index.html
    return render(
        request,
        'check-answer.html',
        context={'question_id': question_id, 'user_choice': user_choice,
                 'active_question': active_question, 'result': result,
                 'next_question_id': next_question_id},
    )



def app_promotion(request):
    """
    View function for home page of site.
    """
    # Render the HTML template index.html
    return render(
        request,
        'app-promo.html',
    )


def welcome(request):
    """
    View function for home page of site.
    """
    # Render the HTML template index.html
    return render(
        request,
        'welcome.html',
    )


def about_me(request):
    """
    View function for home page of site.
    """
    # Render the HTML template index.html
    return render(
        request,
        'about-me.html',
    )