from django.db import models
from datetime import date
from django.urls import reverse #Used to generate URLs by reversing the URL patterns
from django.contrib.auth.models import User #Blog author or commenter


class Question(models.Model):
    """
    Model representing a question(title, choices, answer, difficulty).
    """
    title = models.TextField(max_length=1000, help_text="Enter your question here.")
    choice_a = models.CharField(max_length=200, help_text="Enter choice A.")
    choice_b = models.CharField(max_length=200, help_text="Enter choice B.")
    choice_c = models.CharField(max_length=200, help_text="Enter choice C.")
    choice_d = models.CharField(max_length=200, help_text="Enter choice D.")

    CHOICE_STATUS = (  # choice list for question choice
        ('a', 'Choice A'),
        ('b', 'Choice B'),
        ('c', 'Choice C'),
        ('d', 'Choice D'),
    )

    DIFFICULT_STATUS = (  # difficulty list for question
        ('1', 'Easy'),
        ('2', 'Normal'),
        ('3', 'Hard'),
        ('4', 'Very Hard'),
    )

    correct_choice = models.CharField(max_length=1, choices=CHOICE_STATUS, blank=True, default=None, help_text="Make your choice")
    difficulty = models.CharField(max_length=1, choices=DIFFICULT_STATUS, blank=True, default="1", help_text="Difficulty choice")

    def get_absolute_url(self):
        """
        Returns the url to access a particular blog-author instance.
        """
        return reverse('question-detail', args=[str(self.id)])


    def __str__(self):
        """
        String for representing the Model object.
        """
        return self.title  # [:10] first 10 char we can take